/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.models;

/**
 *
 * @author Mirciony
 */
public class Stup {

    private int id;
    private int idRind;
    private int idPrisaca;
    private int nrFamilie;
    private int anulNasterii;
    private int nrMaticulMatca;
    private int nrStupMama;
    private int totalRame;
    private double putere;
    private double rameCuPuet;
    private double rameCuPolen;
    private double rameCuMiere;
    private double saDatFag;
    private String culoareMatca;
    private String matcaVazuta;
    private String ousoareProapete;
    private String datMicare;
    private String varoua;
    private String botca;
    private String coment;

    public Stup() {
    }

    public Stup(int id, int idRind, int idPrisaca, int nrFamilie, int anulNasterii, int nrMaticulMatca, int nrStupMama, int totalRame, double putere, double rameCuPuet, double rameCuPolen, double rameCuMiere, double saDatFag, String culoareMatca, String matcaVazuta, String ousoareProapete, String datMicare, String varoua, String botca, String coment) {
        this.id = id;
        this.idRind = idRind;
        this.idPrisaca = idPrisaca;
        this.nrFamilie = nrFamilie;
        this.anulNasterii = anulNasterii;
        this.nrMaticulMatca = nrMaticulMatca;
        this.nrStupMama = nrStupMama;
        this.totalRame = totalRame;
        this.putere = putere;
        this.rameCuPuet = rameCuPuet;
        this.rameCuPolen = rameCuPolen;
        this.rameCuMiere = rameCuMiere;
        this.saDatFag = saDatFag;
        this.culoareMatca = culoareMatca;
        this.matcaVazuta = matcaVazuta;
        this.ousoareProapete = ousoareProapete;
        this.datMicare = datMicare;
        this.varoua = varoua;
        this.botca = botca;
        this.coment = coment;
    }

    public String getComent() {
        return coment;
    }

    public void setComent(String coment) {
        this.coment = coment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdRind() {
        return idRind;
    }

    public void setIdRind(int idRind) {
        this.idRind = idRind;
    }

    public int getIdPrisaca() {
        return idPrisaca;
    }

    public void setIdPrisaca(int idPrisaca) {
        this.idPrisaca = idPrisaca;
    }

    public int getNrFamilie() {
        return nrFamilie;
    }

    public void setNrFamilie(int nrFamilie) {
        this.nrFamilie = nrFamilie;
    }

    public int getAnulNasterii() {
        return anulNasterii;
    }

    public void setAnulNasterii(int anulNasterii) {
        this.anulNasterii = anulNasterii;
    }

    public int getNrMaticulMatca() {
        return nrMaticulMatca;
    }

    public void setNrMaticulMatca(int nrMaticulMatca) {
        this.nrMaticulMatca = nrMaticulMatca;
    }

    public int getNrStupMama() {
        return nrStupMama;
    }

    public void setNrStupMama(int nrStupMama) {
        this.nrStupMama = nrStupMama;
    }

    public int getTotalRame() {
        return totalRame;
    }

    public void setTotalRame(int totalRame) {
        this.totalRame = totalRame;
    }

    public double getPutere() {
        return putere;
    }

    public void setPutere(double putere) {
        this.putere = putere;
    }

    public double getRameCuPuet() {
        return rameCuPuet;
    }

    public void setRameCuPuet(double rameCuPuet) {
        this.rameCuPuet = rameCuPuet;
    }

    public double getRameCuPolen() {
        return rameCuPolen;
    }

    public void setRameCuPolen(double rameCuPolen) {
        this.rameCuPolen = rameCuPolen;
    }

    public double getRameCuMiere() {
        return rameCuMiere;
    }

    public void setRameCuMiere(double rameCuMiere) {
        this.rameCuMiere = rameCuMiere;
    }

    public double getSaDatFag() {
        return saDatFag;
    }

    public void setSaDatFag(double saDatFag) {
        this.saDatFag = saDatFag;
    }

    public String getCuloareMatca() {
        return culoareMatca;
    }

    public void setCuloareMatca(String culoareMatca) {
        this.culoareMatca = culoareMatca;
    }

    public String getMatcaVazuta() {
        return matcaVazuta;
    }

    public void setMatcaVazuta(String matcaVazuta) {
        this.matcaVazuta = matcaVazuta;
    }

    public String getOusoareProapete() {
        return ousoareProapete;
    }

    public void setOusoareProapete(String ousoareProapete) {
        this.ousoareProapete = ousoareProapete;
    }

    public String getDatMicare() {
        return datMicare;
    }

    public void setDatMicare(String datMicare) {
        this.datMicare = datMicare;
    }

    public String getVaroua() {
        return varoua;
    }

    public void setVaroua(String varoua) {
        this.varoua = varoua;
    }

    public String getBotca() {
        return botca;
    }

    public void setBotca(String botca) {
        this.botca = botca;
    }

    @Override
    public String toString() {
        return "Stup{" + "id=" + id + ", idRind=" + idRind + ", idPrisaca=" + idPrisaca
                + ", nrFamilie=" + nrFamilie + ", anulNasterii=" + anulNasterii
                + ", nrMaticulMatca=" + nrMaticulMatca + ", nrStupMama=" + nrStupMama
                + ", totalRame=" + totalRame + ", putere=" + putere
                + ", rameCuPuet=" + rameCuPuet + ", rameCuPolen=" + rameCuPolen
                + ", rameCuMiere=" + rameCuMiere + ", saDatFag=" + saDatFag
                + ", culoareMatca=" + culoareMatca + ", matcaVazuta=" + matcaVazuta
                + ", ousoareProapete=" + ousoareProapete + ", datMicare=" + datMicare
                + ", varoua=" + varoua + ", botca=" + botca + ", coment=" + coment + '}';
    }

}
