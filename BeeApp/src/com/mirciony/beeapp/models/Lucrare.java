/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.models;

import java.time.LocalDateTime;

/**
 *
 * @author Mirciony
 */
public class Lucrare {

    private int id;
    private int idStupL;
    private LocalDateTime dataLucrarii;
    private int nrFamilieL;
    private int totalRameL;
    private double putereL;
    private double rameCuPuetL;
    private double rameCuPolenL;
    private double rameCuMiereL;
    private double saDatFagL;
    private String culoareMatcaL;
    private String matcaVazutaL;
    private String ousoareProapeteL;
    private String datMicareL;
    private String varouaL;
    private String botcaL;
    private String gataDeIarnaL;
    private String amUitatL;
    private String comentL;

    public Lucrare() {
    }

    public Lucrare(int id, int idStupL, LocalDateTime dataLucrarii, int nrFamilieL, int totalRameL, double putereL, double rameCuPuetL, double rameCuPolenL, double rameCuMiereL, double saDatFagL, String culoareMatcaL, String matcaVazutaL, String ousoareProapeteL, String datMicareL, String varouaL, String botcaL, String gataDeIarnaL, String amUitatL, String comentL) {
        this.id = id;
        this.idStupL = idStupL;
        this.dataLucrarii = dataLucrarii;
        this.nrFamilieL = nrFamilieL;
        this.totalRameL = totalRameL;
        this.putereL = putereL;
        this.rameCuPuetL = rameCuPuetL;
        this.rameCuPolenL = rameCuPolenL;
        this.rameCuMiereL = rameCuMiereL;
        this.saDatFagL = saDatFagL;
        this.culoareMatcaL = culoareMatcaL;
        this.matcaVazutaL = matcaVazutaL;
        this.ousoareProapeteL = ousoareProapeteL;
        this.datMicareL = datMicareL;
        this.varouaL = varouaL;
        this.botcaL = botcaL;
        this.gataDeIarnaL = gataDeIarnaL;
        this.amUitatL = amUitatL;
        this.comentL = comentL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdStupL() {
        return idStupL;
    }

    public void setIdStupL(int idStupL) {
        this.idStupL = idStupL;
    }

    public LocalDateTime getDataLucrarii() {
        return dataLucrarii;
    }

    public void setDataLucrarii(LocalDateTime dataLucrarii) {
        this.dataLucrarii = dataLucrarii;
    }

    public int getNrFamilieL() {
        return nrFamilieL;
    }

    public void setNrFamilieL(int nrFamilieL) {
        this.nrFamilieL = nrFamilieL;
    }

    public int getTotalRameL() {
        return totalRameL;
    }

    public void setTotalRameL(int totalRameL) {
        this.totalRameL = totalRameL;
    }

    public double getPutereL() {
        return putereL;
    }

    public void setPutereL(double putereL) {
        this.putereL = putereL;
    }

    public double getRameCuPuetL() {
        return rameCuPuetL;
    }

    public void setRameCuPuetL(double rameCuPuetL) {
        this.rameCuPuetL = rameCuPuetL;
    }

    public double getRameCuPolenL() {
        return rameCuPolenL;
    }

    public void setRameCuPolenL(double rameCuPolenL) {
        this.rameCuPolenL = rameCuPolenL;
    }

    public double getRameCuMiereL() {
        return rameCuMiereL;
    }

    public void setRameCuMiereL(double rameCuMiereL) {
        this.rameCuMiereL = rameCuMiereL;
    }

    public double getSaDatFagL() {
        return saDatFagL;
    }

    public void setSaDatFagL(double saDatFagL) {
        this.saDatFagL = saDatFagL;
    }

    public String getCuloareMatcaL() {
        return culoareMatcaL;
    }

    public void setCuloareMatcaL(String culoareMatcaL) {
        this.culoareMatcaL = culoareMatcaL;
    }

    public String getMatcaVazutaL() {
        return matcaVazutaL;
    }

    public void setMatcaVazutaL(String matcaVazutaL) {
        this.matcaVazutaL = matcaVazutaL;
    }

    public String getOusoareProapeteL() {
        return ousoareProapeteL;
    }

    public void setOusoareProapeteL(String ousoareProapeteL) {
        this.ousoareProapeteL = ousoareProapeteL;
    }

    public String getDatMicareL() {
        return datMicareL;
    }

    public void setDatMicareL(String datMicareL) {
        this.datMicareL = datMicareL;
    }

    public String getVarouaL() {
        return varouaL;
    }

    public void setVarouaL(String varouaL) {
        this.varouaL = varouaL;
    }

    public String getBotcaL() {
        return botcaL;
    }

    public void setBotcaL(String botcaL) {
        this.botcaL = botcaL;
    }

    public String getGataDeIarnaL() {
        return gataDeIarnaL;
    }

    public void setGataDeIarnaL(String gataDeIarnaL) {
        this.gataDeIarnaL = gataDeIarnaL;
    }

    public String getAmUitatL() {
        return amUitatL;
    }

    public void setAmUitatL(String amUitatL) {
        this.amUitatL = amUitatL;
    }

    public String getComentL() {
        return comentL;
    }

    public void setComentL(String comentL) {
        this.comentL = comentL;
    }

    @Override
    public String toString() {
        return "Lucrare{" + "id=" + id + ", idStupL=" + idStupL
                + ", dataLucrarii=" + dataLucrarii + ", nrFamilieL=" + nrFamilieL
                + ", totalRameL=" + totalRameL + ", putereL=" + putereL
                + ", rameCuPuetL=" + rameCuPuetL + ", rameCuPolenL=" + rameCuPolenL
                + ", rameCuMiereL=" + rameCuMiereL + ", saDatFagL=" + saDatFagL
                + ", culoareMatcaL=" + culoareMatcaL + ", matcaVazutaL=" + matcaVazutaL
                + ", ousoareProapeteL=" + ousoareProapeteL + ", datMicareL=" + datMicareL
                + ", varouaL=" + varouaL + ", botcaL=" + botcaL + ", gataDeIarnaL=" + gataDeIarnaL
                + ", amUitatL=" + amUitatL + ", comentL=" + comentL + '}';
    }

}
