/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.models;

/**
 *
 * @author Mirciony
 */
public class Rind {

    private int id;
    private int idPrisaca;
    private int nrRind;
    private int nrMaxStRind;
    private int nrTotStRind;

    public Rind() {
    }

    public Rind(int id, int idPrisaca, int nrRind, int nrMaxStRind, int nrTotStRind) {
        this.id = id;
        this.idPrisaca = idPrisaca;
        this.nrRind = nrRind;
        this.nrMaxStRind = nrMaxStRind;
        this.nrTotStRind = nrTotStRind;
    }

    public int getNrTotStRind() {
        return nrTotStRind;
    }

    public void setNrTotStRind(int nrTotStRind) {
        this.nrTotStRind = nrTotStRind;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPrisaca() {
        return idPrisaca;
    }

    public void setIdPrisaca(int idPrisaca) {
        this.idPrisaca = idPrisaca;
    }

    public int getNrRind() {
        return nrRind;
    }

    public void setNrRind(int nrRind) {
        this.nrRind = nrRind;
    }

    public int getNrMaxStRind() {
        return nrMaxStRind;
    }

    public void setNrMaxStRind(int nrMaxStRind) {
        this.nrMaxStRind = nrMaxStRind;
    }

    @Override
    public String toString() {
        return "Rind{" + "id=" + id + ", idPrisaca=" + idPrisaca
                + ", nrRind=" + nrRind + ", nrMaxStRind=" + nrMaxStRind
                + ", nrTotStRind=" + nrTotStRind + '}';
    }

}
