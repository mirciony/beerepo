/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.models;

import java.util.Objects;

/**
 *
 * @author Mirciony
 */
public class Prisaca {

    private int id;
    private String denumire;
    private int nrActualRinduri;
    private int nrActualStupi;
    private int nrMaxRinduri;
    private int nrMaxStupi;

    public Prisaca() {
    }

    public Prisaca(int id, String denumire, int nrActualRinduri, int nrActualStupi, int nrMaxRinduri, int nrMaxStupi) {
        this.id = id;
        this.denumire = denumire;
        this.nrActualRinduri = nrActualRinduri;
        this.nrActualStupi = nrActualStupi;
        this.nrMaxRinduri = nrMaxRinduri;
        this.nrMaxStupi = nrMaxStupi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public int getNrActualRinduri() {
        return nrActualRinduri;
    }

    public void setNrActualRinduri(int nrActualRinduri) {
        this.nrActualRinduri = nrActualRinduri;
    }

    public int getNrActualStupi() {
        return nrActualStupi;
    }

    public void setNrActualStupi(int nrActualStupi) {
        this.nrActualStupi = nrActualStupi;
    }

    public int getNrMaxRinduri() {
        return nrMaxRinduri;
    }

    public void setNrMaxRinduri(int nrMaxRinduri) {
        this.nrMaxRinduri = nrMaxRinduri;
    }

    public int getNrMaxStupi() {
        return nrMaxStupi;
    }

    public void setNrMaxStupi(int nrMaxStupi) {
        this.nrMaxStupi = nrMaxStupi;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + this.id;
        hash = 59 * hash + Objects.hashCode(this.denumire);
        hash = 59 * hash + this.nrActualRinduri;
        hash = 59 * hash + this.nrActualStupi;
        hash = 59 * hash + this.nrMaxRinduri;
        hash = 59 * hash + this.nrMaxStupi;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Prisaca other = (Prisaca) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.nrActualRinduri != other.nrActualRinduri) {
            return false;
        }
        if (this.nrActualStupi != other.nrActualStupi) {
            return false;
        }
        if (this.nrMaxRinduri != other.nrMaxRinduri) {
            return false;
        }
        if (this.nrMaxStupi != other.nrMaxStupi) {
            return false;
        }
        if (!Objects.equals(this.denumire, other.denumire)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Prisaca{" + "id=" + id + ", denumire=" + denumire
                + ", nrActualRinduri=" + nrActualRinduri
                + ", nrActualStupi=" + nrActualStupi + ", nrMaxRinduri=" + nrMaxRinduri
                + ", nrMaxStupi=" + nrMaxStupi + '}';
    }

}
