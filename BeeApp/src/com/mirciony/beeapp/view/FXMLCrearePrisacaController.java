/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.view;

import com.mirciony.beeapp.dao.PrisacaDaoIntf;
import com.mirciony.beeapp.dao.impl.PrisacaDaoImpl;
import com.mirciony.beeapp.models.Prisaca;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Mirciony
 */
public class FXMLCrearePrisacaController implements Initializable {

    private int tfIdPrisaca;
    @FXML
    private TextField tfDenumire;
    @FXML
    private TextField tfNrMaxRinduri;
    @FXML
    private TextField tfNrMaxStupi;

    PrisacaDaoIntf prisacaService;

    Stage stage = new Stage();

    @FXML
    private Button closeButton;

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        prisacaService = new PrisacaDaoImpl();
        clearForm();
    }

    @FXML
    public void handleButtonActionCreazaPrisaca(ActionEvent event) {

        try {
            Prisaca p = readForm();
            prisacaService.adauga(p);
            showMessage(Alert.AlertType.INFORMATION, "Info", "Mesaj Informational", p.getDenumire() + " salvat cu succes in BD ");
            clearForm();
            stage = (Stage) closeButton.getScene().getWindow();
            stage.close();
            new FXMLMainController().handleButtonActionRefresh(event);

        } catch (SQLException ex) {
            showMessage(Alert.AlertType.ERROR, "Error", "A intervenit o erroare", "Eroare la salvare \n" + ex.getMessage());

        }

        System.out.println("xxx");

    }

    private Prisaca readForm() {

        Prisaca p = new Prisaca();
        p.setId(0);
        p.setDenumire(tfDenumire.getText());
        p.setNrMaxRinduri(Integer.parseInt(tfNrMaxRinduri.getText()));
        p.setNrMaxStupi(Integer.parseInt(tfNrMaxStupi.getText()));

        return p;
    }

    private void clearForm() {

        tfDenumire.setText("");
        tfNrMaxRinduri.setText("");
        tfNrMaxStupi.setText("");

    }

    private void showMessage(Alert.AlertType type, String title, String headerText, String contentText) {

        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);

        alert.showAndWait();
    }

    public void showStage() {

        try {
            Parent root = FXMLLoader.load(getClass().getResource("FXMLCrearePrisaca.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("CrearePrisaca");
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
