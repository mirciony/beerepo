/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.view;

import com.mirciony.beeapp.dao.PrisacaDaoIntf;
import com.mirciony.beeapp.dao.impl.PrisacaDaoImpl;
import com.mirciony.beeapp.models.Prisaca;
import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Mirciony
 */
public class FXMLMainController implements Initializable {

    @FXML
    private TableView<Prisaca> tviewPrisaca;
    @FXML
    private TableColumn<Prisaca, String> collPrisacaDenumire;
    @FXML
    private TableColumn<Prisaca, Integer> collPrisacaNrActRinduri;
    @FXML
    private TableColumn<Prisaca, Integer> collPrisacaNrActStupi;
    @FXML
    private TableColumn<Prisaca, Integer> collPrisacaNrMaxRinduri;
    @FXML
    private TableColumn<Prisaca, Integer> collPrisacaNrMaxStupi;
    @FXML
    private TextField tfDenumire;
    @FXML
    private TextField tfNrMaxRinduri;
    @FXML
    private TextField tfNrMaxStupi;
    @FXML
    private TextField tfUltimaLucrare;
    @FXML
    private TextField tfNrActualRinduri;
    @FXML
    private TextField tfNrActualStupi;

    int tfId;

    PrisacaDaoIntf prisacaService;
    ObservableList<Prisaca> prisacaList;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        prisacaService = new PrisacaDaoImpl();
        initializarePrisaca();

    }
    

    @FXML
    private void handleButtonActionCreaza(ActionEvent event) throws InterruptedException {

        new FXMLCrearePrisacaController().showStage();

    }

    @FXML
    private void handleButtonActionSterge(ActionEvent event) {

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation ");
        alert.setHeaderText("Confirmare pentru stergere");
        alert.setContentText("Esti sigur ca doresti sa elimini prisaca din BD?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            int id = tfId;
            String denumire = tfDenumire.getText();
            try {
                Prisaca pp = prisacaService.findById(id);
                if (pp != null) {
                    prisacaService.sterge(pp);
                    showMessage(Alert.AlertType.INFORMATION, "Info", "Mesaj Informational", "Prisaca " + denumire + " a fost eliminata din BD ");
                    refreshLista();
                    clearForm();
                }
            } catch (SQLException ex) {
                showMessage(Alert.AlertType.ERROR, "Error", "A intervenit o eroare", "Nu ati ales prisaca " + denumire + "\n" + ex.getMessage());
            }
        }

    }

    @FXML
    public void handleButtonActionRefresh(ActionEvent event) {

        refreshLista();
        clearForm();

    }

    @FXML
    private void handleButtonActionModifica(ActionEvent event) {
    }

    @FXML
    private void handleButtonActionDetalii(ActionEvent event) {
    }

    @FXML
    private void handleButtonActionMouseClick(MouseEvent event) {

        Prisaca p;
        if (event.getButton().equals(MouseButton.PRIMARY)) {
            fillForm(tviewPrisaca.getFocusModel().getFocusedItem());

            System.out.println("");

        }

    }

    private void showMessage(Alert.AlertType type, String title, String headerText, String contentText) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);

        alert.showAndWait();
    }

    private void fillForm(Prisaca p) {
        tfId = p.getId();
        tfDenumire.setText(String.valueOf(p.getDenumire()));
        tfNrActualRinduri.setText(String.valueOf(p.getNrActualRinduri()));
        tfNrActualStupi.setText(String.valueOf(p.getNrActualStupi()));
        tfNrMaxRinduri.setText(String.valueOf(p.getNrMaxRinduri()));
        tfNrMaxStupi.setText(String.valueOf(p.getNrMaxStupi()));
        tfUltimaLucrare.setText("trebuie de adus data");
    }

    private void initializarePrisaca() {

        prisacaList = FXCollections.observableArrayList(prisacaService.findAll());
        collPrisacaDenumire.setCellValueFactory(new PropertyValueFactory<>("denumire"));
        collPrisacaNrActRinduri.setCellValueFactory(new PropertyValueFactory<>("nrActualRinduri"));
        collPrisacaNrActStupi.setCellValueFactory(new PropertyValueFactory<>("nrActualStupi"));
        collPrisacaNrMaxRinduri.setCellValueFactory(new PropertyValueFactory<>("nrMaxRinduri"));
        collPrisacaNrMaxStupi.setCellValueFactory(new PropertyValueFactory<>("nrMaxStupi"));
        tviewPrisaca.setItems(prisacaList);

    }

    public void refreshLista() {
        try {
            prisacaList = FXCollections.observableArrayList(prisacaService.findAll());
            tviewPrisaca.getItems().clear();
            tviewPrisaca.setItems(prisacaList);
        } catch (Exception ex) {
            showMessage(Alert.AlertType.ERROR, "Error", "A intervenit o eroare", "Nu ati ales prisaca " + ex.getMessage());

        }
    }

    private void clearForm() {

        tfId = 0;
        tfDenumire.setText("");
        tfNrMaxRinduri.setText("");
        tfNrMaxStupi.setText("");
        tfDenumire.setText("");
        tfNrActualRinduri.setText("");
        tfNrActualStupi.setText("");
        tfNrMaxRinduri.setText("");
        tfNrMaxStupi.setText("");
        tfUltimaLucrare.setText("");

    }

}
