/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.view;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Mirciony
 */
public class FXMLModificaPrisacaController implements Initializable {

    @FXML
    private TextField tfIdPrisaca;
    @FXML
    private TextField tfDenumire;
    @FXML
    private TextField tfNrRinduri;
    @FXML
    private TextField tfNrStupuri;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void handleButtonActionModifica(ActionEvent event) {
    }
    
}
