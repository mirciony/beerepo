/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.dao;

import com.mirciony.beeapp.models.Stup;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Mirciony
 */
public interface StupDaoIntf {
    
    void adauga(Stup s) throws SQLException;
    void modifica(Stup s) throws SQLException;
    void sterge(Stup s) throws SQLException;

    Stup findById(int id) throws SQLException;
    Stup findByNrFamilie(int nrFamilie) throws SQLException;

    List<Stup> findAll();
    
}
