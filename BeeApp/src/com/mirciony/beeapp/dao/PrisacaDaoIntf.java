/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.dao;

import com.mirciony.beeapp.models.Prisaca;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Mirciony
 */
public interface PrisacaDaoIntf {

    void adauga(Prisaca p) throws SQLException;
    void modifica(Prisaca p) throws SQLException;
    void sterge(Prisaca p) throws SQLException;

    Prisaca findById(int id) throws SQLException;
    Prisaca findByDenumire(String denumire) throws SQLException;

    List<Prisaca> findAll();

}
