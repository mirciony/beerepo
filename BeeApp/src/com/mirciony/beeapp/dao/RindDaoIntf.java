/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.dao;

import com.mirciony.beeapp.models.Rind;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Mirciony
 */
public interface RindDaoIntf {

    void adauga(Rind r) throws SQLException;
    void modifica(Rind r) throws SQLException;
    void sterge(Rind r) throws SQLException;

    Rind findById(int id) throws SQLException;
    Rind findByNrRind(int nrRind) throws SQLException;
    
    List<Rind> findAll();

}
