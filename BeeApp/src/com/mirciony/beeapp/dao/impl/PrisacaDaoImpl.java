/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.dao.impl;

import com.mirciony.beeapp.dao.MyDataSource;
import com.mirciony.beeapp.dao.PrisacaDaoIntf;
import com.mirciony.beeapp.models.Prisaca;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mirciony
 */
public class PrisacaDaoImpl implements PrisacaDaoIntf {

    private static final Logger LOG = Logger.getLogger(PrisacaDaoImpl.class.getName());

    private final MyDataSource ds = MyDataSource.getInstance();

    @Override
    public void adauga(Prisaca p) throws SQLException {

        String sql = "INSERT INTO prisaca VALUES(null, '" + p.getDenumire() + "', 0, 0,'"
                + p.getNrMaxRinduri() + "','" + p.getNrMaxStupi() + "');";

        try (
                Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();) {
            stat.executeUpdate(sql);
        } catch (Exception e) {
            LOG.severe(e.toString());
            throw new SQLException(e.getMessage());
        }

    }

    @Override
    public void modifica(Prisaca p) throws SQLException {

        String sql = "UPDATE prisaca SET Denumire='" + p.getDenumire()
                + "', NrActRinduriPrisaca='" + p.getNrActualRinduri()
                + "', NrActStupiPrisaca='" + p.getNrActualStupi()
                + "', NrMaxRinduriPrisaca='" + p.getNrMaxRinduri()
                + "', NrMaxStupiPrisaca='" + p.getNrMaxStupi()
                + "' WHERE Id = " + p.getId();

        try (
                Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();) {
            stat.executeUpdate(sql);
        } catch (Exception e) {
            LOG.severe(e.toString());
            throw new SQLException(e.getMessage());
        }

    }

    @Override
    public void sterge(Prisaca p) throws SQLException {

        String sql = "DELETE FROM prisaca WHERE Id=" + p.getId();

        try (
                Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();) {

            stat.executeUpdate(sql);

        } catch (Exception e) {

            LOG.severe(e.toString());
            throw new SQLException(e.getMessage());
        }

    }

    @Override
    public Prisaca findById(int id) throws SQLException {

        String sql = "SELECT * FROM prisaca WHERE Id = " + id;
        Prisaca prisaca = null;
        try (
                Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();
                ResultSet rs = stat.executeQuery(sql);) {
            if (rs.next()) {
                String denumire = rs.getString(2);
                int nrActualRinduri = rs.getInt(3);
                int nrActualStupi = rs.getInt(4);
                int nrMaxRinduri = rs.getInt(5);
                int nrMaxStupi = rs.getInt(6);

                prisaca = new Prisaca(id, denumire, nrActualRinduri, nrActualStupi, nrMaxRinduri, nrMaxStupi);
            } else {
                throw new SQLException("Nu exista asa prisaca!");
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        }
        return prisaca;
    }

    @Override
    public Prisaca findByDenumire(String denumire) throws SQLException {

        String sql = "SELECT * FROM prisaca WHERE Denumire = " + denumire;
        Prisaca prisaca = null;
        try (
                Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();
                ResultSet rs = stat.executeQuery(sql);) {
            if (rs.next()) {
                int id = rs.getInt(1);
                int nrActualRinduri = rs.getInt(3);
                int nrActualStupi = rs.getInt(4);
                int nrMaxRinduri = rs.getInt(5);
                int nrMaxStupi = rs.getInt(6);

                prisaca = new Prisaca(id, denumire, nrActualRinduri, nrActualStupi, nrMaxRinduri, nrMaxStupi);
            } else {
                throw new SQLException("Nu exista asa prisaca!");
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        }
        return prisaca;
    }

    @Override
    public List<Prisaca> findAll() {

        String sqlsb = "SELECT * FROM prisaca";

        List<Prisaca> lista = new ArrayList<>();

        try (
                Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();
                ResultSet rs = stat.executeQuery(sqlsb);) {
            while (rs.next()) {
                int id = rs.getInt(1);
                String denumire = rs.getString(2);
                int nrActualRinduri = rs.getInt(3);
                int nrActualStupi = rs.getInt(4);
                int nrMaxRinduri = rs.getInt(5);
                int nrMaxStupi = rs.getInt(6);

                Prisaca prisaca = new Prisaca(id, denumire, nrActualRinduri, nrActualStupi, nrMaxRinduri, nrMaxStupi);
                lista.add(prisaca);
            }

        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
        }

        return lista;
    }

}
