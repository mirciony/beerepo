/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.dao.impl;

import com.mirciony.beeapp.dao.MyDataSource;
import com.mirciony.beeapp.dao.RindDaoIntf;
import com.mirciony.beeapp.models.Rind;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mirciony
 */
public class RindDaoImpl implements RindDaoIntf {

    private static final Logger LOG = Logger.getLogger(RindDaoImpl.class.getName());

    private final MyDataSource ds = MyDataSource.getInstance();

    @Override
    public void adauga(Rind r) throws SQLException {

        String sql = "INSERT INTO rind VALUES(null,'" + r.getIdPrisaca() + "', '"
                + r.getIdPrisaca() + "', '" + r.getNrRind() + "', '"
                + r.getNrMaxStRind() + "', '" + r.getNrTotStRind() + "');";

        try (
                Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();) {
            stat.executeUpdate(sql);
        } catch (Exception e) {
            LOG.severe(e.toString());
            throw new SQLException(e.getMessage());
        }
    }

    @Override
    public void modifica(Rind r) throws SQLException {

        String sql = "UPDATE rind SET IdPrisaca='" + r.getIdPrisaca()
                + "', NrRind='" + r.getNrRind()
                + "', NrMaxStupiRind='" + r.getNrMaxStRind()
                + "', NrTotalStupiRind='" + r.getNrTotStRind()
                + "' WHERE Id = " + r.getId();

        try (
                Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();) {
            stat.executeUpdate(sql);
        } catch (Exception e) {
            LOG.severe(e.toString());
            throw new SQLException(e.getMessage());
        }

    }

    @Override
    public void sterge(Rind r) throws SQLException {

        String sql = "DELETE FROM rind WHERE Id=" + r.getId();

        try (
                Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();) {

            stat.executeUpdate(sql);

        } catch (Exception e) {

            LOG.severe(e.toString());
            throw new SQLException(e.getMessage());
        }

    }

    @Override
    public Rind findById(int id) throws SQLException {

        String sql = "SELECT * FROM rind WHERE Id = " + id;
        Rind rind = null;
        try (
                Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();
                ResultSet rs = stat.executeQuery(sql);) {
            if (rs.next()) {
                int idPrisaca = rs.getInt(2);
                int nrRind = rs.getInt(3);
                int nrMaxStRind = rs.getInt(4);
                int nrTotStRind = rs.getInt(5);

                rind = new Rind(id, idPrisaca, nrRind, nrMaxStRind, nrTotStRind);
            } else {
                throw new SQLException("Nu exista asa prisaca!");
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        }
        return rind;

    }

    @Override
    public Rind findByNrRind(int nrRind) throws SQLException {

        String sql = "SELECT * FROM rind WHERE NrRind = " + nrRind;
        Rind rind = null;
        try (
                Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();
                ResultSet rs = stat.executeQuery(sql);) {
            if (rs.next()) {
                int id = rs.getInt(1);
                int idPrisaca = rs.getInt(2);
                int nrMaxStRind = rs.getInt(4);
                int nrTotStRind = rs.getInt(5);

                rind = new Rind(id, idPrisaca, nrRind, nrMaxStRind, nrTotStRind);
            } else {
                throw new SQLException("Nu exista asa prisaca!");
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw new SQLException(ex.getMessage());
        }
        return rind;

    }

    @Override
    public List<Rind> findAll() {

        String sqlsb = "SELECT * FROM prisaca";

        List<Rind> lista = new ArrayList<>();

        try (
                Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();
                ResultSet rs = stat.executeQuery(sqlsb);) {
            while (rs.next()) {
                int id = rs.getInt(1);
                int idPrisaca = rs.getInt(2);
                int nrRind = rs.getInt(3);
                int nrMaxStRind = rs.getInt(4);
                int nrTotStRind = rs.getInt(5);

                Rind rind = new Rind(id, idPrisaca, nrRind, nrMaxStRind, nrTotStRind);
                lista.add(rind);
            }

        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
        }

        return lista;

    }

}
