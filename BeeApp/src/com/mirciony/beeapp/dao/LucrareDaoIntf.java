/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.dao;

import com.mirciony.beeapp.models.Lucrare;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author Mirciony
 */
public interface LucrareDaoIntf {
    
    void adauga(Lucrare l) throws SQLException;
    void modifica(Lucrare l) throws SQLException;
    void sterge(Lucrare l) throws SQLException;

    Lucrare findById(int id) throws SQLException;
    Lucrare findByIdStup(int idStupL) throws SQLException;
    Lucrare findByNrFamilie(int nrFamilie) throws SQLException;
    Lucrare findByDate(LocalDateTime dataLucrarii) throws SQLException;
    
    List<Lucrare> findAll();
    
}
