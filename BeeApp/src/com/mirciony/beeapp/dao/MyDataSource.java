/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mirciony.beeapp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mirciony
 */
public class MyDataSource {

    private static final Logger LOG = Logger.getLogger(MyDataSource.class.getName());

    String driverName = "com.mysql.jdbc.Driver";
    String urlDB = "jdbc:mysql://localhost:3306/beedb";
    String dbuser = "user";
    String password = "123456789";

    Connection connection;

    private MyDataSource() {

        try {
            loadProperties();
            loadDriver();
            testConection();
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }

    }

    private void loadProperties() {

        LOG.info("Proprietatile bazei au fost incarcate!");
        System.out.println("aaa   " + urlDB + dbuser + password);
    }

    private void loadDriver() throws ClassNotFoundException {

        Class.forName(driverName);
        LOG.info("Driver " + driverName + " incarcat cu succes!");

    }

    private void testConection() throws SQLException {

        connection = DriverManager.getConnection(urlDB, dbuser, password);
        LOG.info("Conexiunea creata cu succes!");

    }

    public Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(urlDB, dbuser, password);
        }

        return connection;
    }

    public static MyDataSource getInstance() {
        return MyDataSourceHolder.INSTANCE;
    }

    private static class MyDataSourceHolder {

        private static final MyDataSource INSTANCE = new MyDataSource();
    }

}
